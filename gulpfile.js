'use strict';
var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    notify = require("gulp-notify"),
    rimraf = require('gulp-rimraf'),
    concat = require('gulp-concat'),
    argv = require('yargs').argv,
    replace = require('gulp-replace'),
    beautify = require('gulp-beautify'),
    rename = require("gulp-rename"),
    runSequence = require('run-sequence'),
    header = require('gulp-header'),
    fs = require('fs'),
    prompt = require('gulp-prompt'),
    buildManifest = require("./src/buildManifest.json"),
    pkg = require('./package.json');

var globalParts, globalPartsArray = [], manifestInput;



gulp.task('concatjs', function () {
    var filesToConcat = [];
    if (globalPartsArray.length > 0) {
        filesToConcat = globalPartsArray;
    } else {
        filesToConcat = ['./src/parts/*.js'];

    }
    return gulp.src(filesToConcat)
        .pipe(concat('tmp.js'))
        .pipe(gulp.dest('./tmp/'));
});

gulp.task('replacePlaceholder', ['concatjs'], function () {
    return gulp.src(['./src/_placeholder.js'])
        .pipe(replace("__Parts__", function (match, p1) {
        return fs.readFileSync('./tmp/tmp.js');
    }))
        .pipe(gulp.dest('./tmp/'));
});


gulp.task('beautifyJs', ['replacePlaceholder'], function () {
    return gulp.src('./tmp/_placeholder.js')
        .pipe(beautify({
        indentSize: 2
    }))
        .pipe(gulp.dest('./tmp/'))
});


gulp.task('renamePlaceholder', ['beautifyJs'], function () {
    var name = "Scissors.js";
    if (globalPartsArray.length > 0) {
        name = "Scissors.custom-build.js"
    }
    return gulp.src("./tmp/_placeholder.js")
        .pipe(rename(name))
        .pipe(gulp.dest("./final"));
});

gulp.task('uglifyJs', ['renamePlaceholder'], function () {
    return gulp.src('./tmp/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('./tmp/'));
});


gulp.task('moveMinJs', ['uglifyJs'], function () {
    var name = "Scissors.min.js";
    if (globalPartsArray.length > 0) {
        name = "Scissors.custom-build.min.js";
    }
    return gulp.src("./tmp/_placeholder.js")
        .pipe(rename(name))
        .pipe(gulp.dest("./final"));
});


gulp.task('cleanUnused', ['moveMinJs'], function () {
    var msg = "Ahoy! Scissors build succeded!";
    if (globalPartsArray.length > 0) {
        msg = "Ahoy! Scissors custom build succeded! Parts used: " + globalParts;
    }

    gulp.src(["./final/Scissors.min.js","./final/Scissors.js"])
        .pipe(gulp.dest("./"));

    return gulp.src(['./tmp/'], {
            read: false
        })
        .pipe(rimraf())
        .pipe(notify(msg));
});


gulp.task('addHeaders', ['cleanUnused'], function () {
    //first add the production header
    var productionFile = "./final/Scissors.min.js",
        devFile = "./final/Scissors.js",
        devHeaderStr, prodHeaderStr,
        generated, generatedStr;
    generated = new Date();
    generatedStr = generated.toGMTString();
    devHeaderStr = ['/**',
        ' * <%= pkg.name %>',
        ' * <%= pkg.credits %>',
        ' * Generated: <%= generatedStr %>',
        ' * @version v<%= pkg.version %>',
        ' * @link <%= pkg.url %>',
        ' * @license <%= pkg.licenses[0].type %> | <%= pkg.licenses[0].url %>',
        ' */',
        ''].join('\n');
    prodHeaderStr = ['/**',
        ' * <%= pkg.name %> v<%= pkg.version %> <%= pkg.developmentDesc %>',
        '*/',
        ''].join('\n');

    if (globalPartsArray.length > 0) {
        productionFile = "./final/Scissors.custom-build.min.js";
        devFile = "./final/Scissors.custom-build.js";
        devHeaderStr = ['/**',
            ' * <%= pkg.name %> (Custom build)',
            ' * <%= pkg.credits %>',
            ' * Build: <%= globalParts %>',
            ' * Generated: <%= generatedStr %>',
            ' * @version v<%= pkg.version %>',
            ' * @link <%= pkg.url %>',
            ' * @license <%= pkg.licenses[0].type %> | <%= pkg.licenses[0].url %>',
            ' */',
            ''].join('\n');
        prodHeaderStr = ['/**',
            ' * <%= pkg.name %> v<%= pkg.version %> (Custom Build) <%= pkg.developmentDesc %>',
            '*/',
            ''].join('\n');
    }
    gulp.src(devFile)
        .pipe(header(devHeaderStr, {
        pkg: pkg,
        generatedStr: generatedStr,
        globalParts: globalParts
    }))
        .pipe(gulp.dest('./final/'));

    return gulp.src(productionFile)
        .pipe(header(prodHeaderStr, {
        pkg: pkg,
        generatedStr: generatedStr,
        globalParts: globalParts
    }))
        .pipe(gulp.dest('./final/'));

});


gulp.task('default', ['concatjs', 'replacePlaceholder', 'beautifyJs', 'renamePlaceholder', 'uglifyJs', 'moveMinJs', 'addHeaders', 'cleanUnused']);

gulp.task('build-by', function () {
    globalParts = argv.parts;

    if (globalParts !== undefined) globalPartsArray = globalParts.split(",");

    for (var i = 0; i < globalPartsArray.length; i++) {
        globalPartsArray[i] = './src/parts/' + globalPartsArray[i].trim() + '.js';
    }

    runSequence(['concatjs', 'replacePlaceholder', 'beautifyJs', 'renamePlaceholder', 'uglifyJs', 'moveMinJs', 'addHeaders', 'cleanUnused'], function () {
        console.log("Custom build ended");
    });

});


gulp.task('build-custom', function () {
	manifestInput = argv.manifest;
	if(manifestInput !== undefined && buildManifest[manifestInput] !== undefined)
	    globalParts = buildManifest[manifestInput].join(",");

    if (globalParts !== undefined) globalPartsArray = globalParts.split(",");

    for (var i = 0; i < globalPartsArray.length; i++) {
        globalPartsArray[i] = './src/parts/' + globalPartsArray[i].trim() + '.js';
    }

    runSequence(['concatjs', 'replacePlaceholder', 'beautifyJs', 'renamePlaceholder', 'uglifyJs', 'moveMinJs', 'addHeaders', 'cleanUnused'], function () {
        console.log("Custom build ended");
    });

});



/* Ask and build task */

gulp.task('build', function () {
    //first read the files
    
    var partsAvailable = getFiles('./src/parts');

    //split and get last the filename
    
    partsAvailable.forEach(function(part,key) {
        partsAvailable[key] = part.replace(/^.*[\\\/]/, '').replace(".js", '');
        
    });
    return gulp.src('./src/parts/*.js')
    .pipe(prompt.prompt([{
        type: 'checkbox',
        name: 'partsToBuild',
        message: 'Select the components for your Scissors build:',
        choices: partsAvailable
    }], function(res){
         for (var i = 0; i < res.partsToBuild.length; i++) {
            globalPartsArray[i] = './src/parts/' + res.partsToBuild[i].trim() + '.js';
         }
         globalParts = res.partsToBuild.join(",");
         runSequence(['concatjs', 'replacePlaceholder', 'beautifyJs', 'renamePlaceholder', 'uglifyJs', 'moveMinJs', 'addHeaders', 'cleanUnused'], function () {
         
         });
    }));

});

/* watch for dev reasons */

gulp.task('watch', function () {
    gulp.watch('./src/parts/*.js', ['concatjs', 'replacePlaceholder', 'beautifyJs', 'renamePlaceholder', 'uglifyJs', 'moveMinJs', 'addHeaders', 'cleanUnused']);
    
});



/* Utility function / file name reader */

function getFiles (dir, files_){
    files_ = files_ || [];
    var files = fs.readdirSync(dir);
    for (var i in files){
        var name = dir + '/' + files[i];
        if (fs.statSync(name).isDirectory()){
            getFiles(name, files_);
        } else {
            files_.push(name);
        }
    }
    return files_;
}
