#Scissors.js - blazing fast JavaScript. Use only what you need. Vanilla flavored!
Scissors is a JavaScript library that provides a bunch of useful functions as a replacement of jQuery, without extending or mutating any any built-in objects. Inspired by [You might not need jQuery](http://youmightnotneedjquery.com/)  and similar frameworks like [Zepto.js](http://zeptojs.com/), Scissors allows you use plain vanilla flavored JavaScript out of the box. Furthermore, you may use Scissor's Gulp builder, in order to customize your build and improve your working process. Fast and efficient, Scissors will be your swiss army knife in your next project.

###Browser Support
IE: 9+
Safari: 7+
Firefox: 31+
Chrome: 31+
Android Browser: 4.1+
Chrome for Android: 42+
iOS Safari: 7.1+

*IE8 is partially supported. If you want to support outdated browsers, you can use jQuery 1.x.x versions or Scissors with the following polyfills:
[querySelector](https://gist.github.com/chrisjlee/8960575)
[querySelectorAll](https://gist.github.com/connrs/2724353)
[es5-shim](https://github.com/es-shims/es5-shim)
You may struggle to make this think work though.

###Usage
Append Scissors.js into the `<head />` of your HTML document and you are ready to go.

###Quick intro
Scissors.js is build using modular components. To be more accurate, Scissors in nothing more than a set of efficient functions wrapped around a global accessible prototype. You may use it's methods, directly into your code. Methods that use DOM elements can be accessed directly using string expression selectors or even element references:
`
var element = document.getElementById("myid")
var mutatedElement = Scissors.addClass(element,"myclass1 myclass2");
var appendedClasses = Scissors.addClass("div","myclass3 myclass4");
`

Chaining methods is pretty sweet too, furthermore, callback functions are mostly out of the box:
`Scissors.addClass("div","class1",function(elements){ console.log("elements affected ->:", elements)});`





####Build Usage
You may create your own builds using Gulp. The process is quite simple.
1) Create a folder for your build `mkdir scissors && cd scissors`
2) Install npm and node as described [here](https://nodejs.org/)
3) Clone the repo `git clone https://github.com/vorillaz/scissors.git .`
4) Install any missing dependencies using: `npm install`
5) Build Scissors.js as : `gulp` or `gulp default`. You can see your building genereted files Scissors.js and Scissors.min.js into the final folder.

####Custom Builds
You may also create a custom build of Scissors.js using Gulp. 
Simply type `gulp build`  into your terminal.
A "Yeoman-like" generator will pop up. Pick up your desirable components and you are ready to go.
Your generated build files could be found into final folder as Scissors.custom-build.js and Scissors.custom-build.min.js

####Build Scissors using direct input
Scissors can be built using direct input from your command line.
`gulp build-by --parts=[part1,part2,part3...]`. 
For instance, you may create a build using Scissors'  `addClass()`, `removeClass()`, `hasClass()` and `hide()` methods as `gulp build --parts=addClass,removeClass,hasClass,hide`

Your generated build files could be found into final folder as Scissors.custom-build.js and Scissors.custom-build.min.js

###Build cheatsheet and shorthands:
Inside `src` folder there is a manifest file (buildManifest.json) containing groupings of custom builds.
You may use them as `gulp build-custom --manifest=ui` which will generate a Scissors custom build containing `Scissors.hide(), Scissors.show(), Scissors.fadeIn(), Scissors.fadeOut(), Scissors.getCss(), Scissors.getAttr() and Scissors.setAttr()`

You may also create your own shorthands simply by extending the buildManifest.json file

###Available shorthands
`gulp build-custom --manifest=ui` ⟶ `["hide","show","fadeIn","fadeOut","getCss","getAttr","setAttr"]`

`gulp build-custom --manifest=class` ⟶  `["removeClass","addClass","toggleClass","hasClass"]`

`gulp build-custom --manifest=utilities` ⟶  [`"filter","trim","each"]`

`gulp build-custom --manifest=events` ⟶  `["documentReady","trigger","on","off"]`

`gulp build-custom --manifest=query` ⟶`["before","find","after","clone","parent","contains"]`

###Table of Content
* [Scissors.addClass()](#addClass)
* [Scissors.after()](#after)
* [Scissors.before()](#before)
* [Scissors.clone()](#clone)
* [Scissors.contains()](#contains)
* [Scissors.createCache()](#createCache)
* [Scissors.js()](#js)
* [Scissors.documentReady()](#documentReady)
* [Scissors.each()](#each)
* [Scissors.fadeIn()](#fadeIn)
* [Scissors.fadeOut()](#fadeOut)
* [Scissors.filter()](#filter)
* [Scissors.find()](#find)
* [Scissors.get()](#get)
* [Scissors.getAttr()](#getAttr)
* [Scissors.getCache()](#getCache)
* [Scissors.getCss()](#getCss)
* [Scissors.getHeight()](#getHeight)
* [Scissors.getMaxHeight()](#getMaxHeight)
* [Scissors.getMaxWidth()](#getMaxWidth)
* [Scissors.getWidth()](#getWidth)
* [Scissors.hasClass()](#hasClass)
* [Scissors.hide()](#hide)
* [Scissors.inArray()](#inArray)
* [Scissors.isDomObject()](#isDomObject)
* [Scissors.next()](#next)
* [Scissors.off()](#off)
* [Scissors.offset()](#offset)
* [Scissors.on()](#on)
* [Scissors.parent()](#parent)
* [Scissors.remove()](#remove)
* [Scissors.removeClass()](#removeClass)
* [Scissors.setAttr()](#setAttr)
* [Scissors.setHtml()](#setHtml)
* [Scissors.show()](#show)
* [Scissors.toggle()](#toggle)
* [Scissors.toggleClass()](#toggleClass)
* [Scissors.trigger()](#trigger)
* [Scissors.trim()](#trim)
* [Scissors.windowLoad()](#windowLoad)


####Scissors.addClass(elements, classes, callback)
Adds the specified class(es) to each element in the set of matched elements.
*returns the elements affected*

```	
	var elmnt = document.getElementsByTagName("div");
	var elmntSecond = document.getElementById("test");
	
	Scissors.addClass(elmnt, "test-class");
	Scissors.addClass(elmntSecond, "test-class-second");
	Scissors.addClass("span", "test-class-second",function(elementsAffected){
		console.log(elementsAffected)
	});
```

####Scissors.after(elements, content, callback)
Insert content, specified by the parameter, after each element in the set of matched elements.

```	
	var elmnt = document.getElementsByTagName("div");
	var elmntSecond = document.getElementById("test");
	
	Scissors.after(elmnt, "I am a string");
	Scissors.after(elmntSecond, "<p>I am a paragraph</p>");
	Scissors.after("span", ":)",function(elementsAffected){
		console.log(elementsAffected)
	});
```

*returns the elements affected*

####Scissors.before(elements, content, callback)
Insert content, specified by the parameter, before each element in the set of matched elements.

```	
	var elmnt = document.getElementsByTagName("div");
	var elmntSecond = document.getElementById("test");
	
	Scissors.before(elmnt, "I am a string");
	Scissors.before(elmntSecond, "<p>I am a paragraph</p>");
	Scissors.before("span", ":)",function(elementsAffected){
		console.log(elementsAffected)
	});
```

*returns the elements affected*

####Scissors.clone(elements, callback)
Clone the set of matched elements.

```	
	var elmnt = document.getElementsByTagName("div");
	var elmntSecond = document.getElementById("test");
	
	var clone = Scissors.clone(elmnt);
	var returnOfTheClones = Scissors.clone(elmntSecond);
	var jedis =Scissors.clone(".hank-solo",function(elementsAffected){
		console.log(elementsAffected)
	});
```

*returns the elements affected*

####Scissors.contains(elements,selector)
Check if the set of matched elements contains any elements matched by the selector.

```	
	var elmnt = document.getElementsByTagName("div");
	
	if(Scissors.contains(elmnt,"em")){
		console.log("Ahoy!")
	}
	
	if(Scissors.contains("span:first-of-type",".test")){
		console.log("Ahoy x2!")
	} else{
		console.log("Shipwreck")
	}
```

*returns boolean*

####Scissors.createCache(query)
The only ####Scissors.js dependency. Matches any document element against the query and returns them directly or via Scissors' caching system. Cache's refreshed on each pageload.

```	
	//store this ish
	Scissors.createCache("span");
	//extra code
	var cached = Scissors.getCache("span");
```

*returns the referenced elements*

####Scissors.documentReady(callback)
Specify a function to execute when the DOM is fully loaded.

```	Scissors.documentReady(function(){
		alert("DOM is doomed!");
	});
```
 
####Scissors.each(elements, callback)
Evaluates the callback against the set of matched elements.

```	Scissors.each([1,2,3],function(i,v){
		console.log(i,v)
	});
```

####Scissors.fadeIn(elements, duration, display, callback)
Display the matched elements by fading them to opaque.

```
	var elmnt = document.getElementsByTagName("div");
	Scissors.fadeIn(elmnt)
	Scissors.fadeIn("span", 1000); //execute in 1000ms
	
	Scissors.fadeIn("em > .test", 1000, "inline-block"); //display inline block

	Scissors.fadeIn(".div", 1000, "inline-block", function(elementsAffected){
		console.log(elementsAffected)
	});
```

*returns the elements affected*

####Scissors.fadeOut(elements, duration)
Hide the matched elements by fading them to transparent.

```
	var elmnt = document.getElementsByTagName("div");
	Scissors.fadeOut(elmnt)
	Scissors.fadeOut("span", 1000); //execute in 1000ms
	
	Scissors.fadeOut(".div", 1000, function(elementsAffected){
		console.log(elementsAffected)
	});
```

*returns the elements affected*

####Scissors.filter(elements,callback)
Reduce the set of matched elements to those that match the selector or pass the function’s test.

```
	Scissors.filter("span",function(v,i){
	  v.innerHTML = "Scissors R O C K S"
	});
```

*returns the elements matched or false if no elements were filtered*

####Scissors.find(elements, selector, callback)
Get the descendants of each element in the current set of matched elements, filtered by a selector, jQuery object, or element.

```
	var elmnt = document.getElementsByTagName("div");
	
	console.log(Scissors.find(elmnt,"em"));
	Scissors.each(Scissors.find("span","sup"),function(v,i){
		console.log(v.innerHTML = "Pedro")
	});
```

*returns the elements matched*

####Scissors.get( query)
Get current set of matched elements from string selector query

```
	var cached = Scissors.getCache("#heyId");
	var elements = Scissors.getCache(".heyClass");
	console.log(elements.length, " ;) ")```

*returns the elements matched*

####Scissors.getAttr( elements, attribute(s) , callback)
Get the value of an attribute for the current set of matched elements

```
	var elmnt = document.getElementsByTagName("div");
	
	Scissors.getAttr(elmnt,"data-id",function(attrs){
		Scissors.each(attrs,function(v,i){
			console.log("El: ", v.element);
			console.log("Attributes: ", v.attributes);
		})
	})```

*returns an array of attributes and elements referenced*

####Scissors.getCache(query)
Retrieve any matched set of elements stored in Scissors' temporary cache.

```
	var cached = Scissors.getCache("span");
	var cachedItalics = Scissors.getCache("em");
```

*returns an array of matched elements in the cache object*

####Scissors.getCss(elements, styles)
Filter and get the specified styles out of the the set of matched elements

```
	var colored = Scissors.getCss("span","color");
	console.log(colored.styles.color);
```

*returns an array of objects fetching the affected elements as well as the matched CSS styles*

####Scissors.getHeight(elements, callback)
Get the current computed height for the current set of matched elements.

```
	var tall = Scissors.getHeight("span");
	console.log("The first element is :" + tall[0]["element"] + " and is sooooooo tall :" + tall[0]["height"]);
```

*returns an array of objects fetching the computed heights as well as the affected elements*

####Scissors.getMaxHeight(elements,callback)
Find the maximum calculated height against the current set of matched elements.

```
	var maxH = Scissors.getMaxHeight("span");
	console.log("The tallest span is :" + maxH["element"] + " WE CALL IT GULLIVER AND :" + maxH["maxHeight"]);
```

*returns an array of objects fetching the maximum height as well as the affected element*

####Scissors.getMaxWidth(elements,callback)
Get the current computed height for the current set of matched elements.

```
	var FAT = Scissors.getMaxWidth("span");
	console.log("Fat span  :" + FAT["element"] + " is sexy and weights :" + FAT["maxWidth"]);
```

*returns an array of objects fetching the computed heights as well as the affected element*

####Scissors.getWidth(elements,callback)
Get the current computed width for the current set of matched elements.

```
	var fatBoySlim = Scissors.getWidth("span");
	console.log(fatBoySlim[0]);
```

*returns an array of objects fetching the computed widths as well as the affected element*


####Scissors.hasClass(elements, class(es))
Determine whether any of the matched elements are assigned the given class(es).

```	var elmnt = document.getElementsByTagName("div");
	
	if(Scissors.hasClass("test")){
		alert("passed");
	}```

*returns boolean*

####Scissors.hide(elements, callback)
Hide the matched set of elements.

```   Scissors.hide(".hideAndSeek",function(e){
		console.log(e);
	});
```

*returns the hidden set of elements*

####Scissors.inArray(array, needle)
Looks through each value in the array, returning the position of the needle in the array  or false if the needle could not be found. 

```
	if(Scissors.inArray([1,2,3],1)){
		console.log("Easy!");
	}
```

*returns index of false*

####Scissors.isDomObject(object)
Test if an object is DOM related

```
	var elmnt = document.createElement("div");
	if(Scissors.isDomObject(elmnt)){
		console.log("DONT MESS W/ THE DOM");
	}
```

*returns boolean*

####Scissors.off(elements, callback)
Get the immediately following sibling of each element in the set of matched elements.

```
	console.log(Scissors.next("span"));
	Scissors.next("span", function(el){
		console.log(el[0].element, el[0].next);
	})
```

*returns an of objects containing the set of elements matched as well as their siblings*

####Scissors.off(elements, eventName ,callback)
Remove an event handler from the matched set of elements.

```
	Scissors.off("span","click" function(el){
		//do nothing
	});
```

*returns the set of elements affected*



####Scissors.offset(elements, callback)
Get the offeset coordinates of the the matched set of elements.

```Scissors.offset("span", function(el){
		console.log(el[0].rect);
		// bottom: xx, height: xx, left: xx, right: xx, top: xx, width: xx
	});
```

*returns an array with the coordinates of each element as well as the referenced elements*

####Scissors.on(elements, eventName ,callback)
Add an event handler from the matched set of elements.

```
	Scissors.on("#btn", "click", function(el){
		alert("Click!")
	});
```

*returns the set of elements affected*


####Scissors.parent(elements, callback)
Get the parent(s) of each element in the current set of matched elements
*returns an array consisting of the selected set of elements paired with their parent elements*


####Scissors.remove(elements, callback)
Removes the matched set of elements from the DOM

```
	Scissors.remove("#itemToRemove", function(removedItem){
		console.log("Bye!", removedItem);
	});
```

*returns the set of elements affected*

####Scissors.removeClass(elements, classes, callback)
Adds the specified class(es) to each element in the set of matched elements.

```
	var elmnt = document.getElementsByTagName("div");
	var elmntSecond = document.getElementById("test");
	
	Scissors.removeClass(elmnt, "test-class");
	Scissors.removeClass(elmntSecond, "test-class-second");
	Scissors.removeClass("span", "test-class-second",function(elementsAffected){
		console.log(elementsAffected)
	});
```

*returns the set of elements affected*

####Scissors.setAttr(elements, attribute, setter,callback)
Set one or more attributes for every matched element

```
	var elmnt = document.getElementsByTagName("div");
	var elmntSecond = document.getElementById("test");
	
	Scissors.setAttr(elmnt, "data-id", "#dataCustomId");
	Scissors.setAttr(elmntSecond, "data-id", "#dataCustomId");
	Scissors.setAttr("span", "data-id", "#dataCustomId", function(elementsAffected){
		console.log(elementsAffected)
	});
```

*returns the set of elements affected*

####Scissors.setHtml(elements, value, callback)
Set the HTML contents of every matched element

```
	Scissors.setHtml("div","<h1>Heading</h1>");
```

*returns the set of elements affected*

####Scissors.show(elements, callback)
Display the matched set of elements.

```
	Scissors.show(".showMe", function(elementsAffected){
		console.log(elementsAffected)
	});
```

*returns the affected set of elements*

####Scissors.toggle(elements , callback)
Reverse the visibility of each element in the set of matched elements

```
	Scissors.toggle(".toggleMe", function(elementsAffected){
		console.log(elementsAffected)
	});
```

*returns the set of elements affected*

####Scissors.toggleClass(elements , class(es) ,callback)
Add or remove one or more classes from each element in the set of matched elements

```
	Scissors.toggleClass(".toggleMyClass", "sweet sour" , function(elementsAffected){
		console.log(elementsAffected)
	});
```

*returns the set of elements affected*

####Scissors.trigger(elements, eventName)
Fire the specified event, binded on the set of the matched elements.

```
	Scissors.toggleClass(".teaseMe", "click");
```

*returns the set of elements affected*

####Scissors.trim(text)
Remove the whitespace from the beginning and end of a string.

```
	Scissors.toggleClass("   Trrrrrrimmmmm ");
```
*returns the string affected*

####Scissors.windowLoad(callback)
Specify a function to execute when the window is fully loaded.

```
	Scissors.windowLoad(function(){
		alert("Loaded!")
	});
```










