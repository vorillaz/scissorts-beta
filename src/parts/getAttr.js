Scissors.getAttr = function(elem,attribute,callback){
    var i = 0,
        tmpobj = {},
        attrs = [],
        returnedArray = [],
        arrayofAttrs = [],
        elem = typeof elem === "string" ? this.createCache(elem) : 
        (typeof elem === "object" && ! elem.length ? [elem] : elem),
        l = elem.length;
    if(elem.length && typeof attribute === "string" && attribute){
        arrayofAttrs = attribute.split(",");

        
            for ( ; i < l; i++ ) {
                tmpobj = {};
                tmpobj.element = elem[i]; 
                attrs = [];
                for (var j = arrayofAttrs.length - 1; j >= 0; j--) {
                    attrs[arrayofAttrs[j]] = elem[i].getAttribute(arrayofAttrs[j]);
                }
                tmpobj.attributes = attrs;    
            returnedArray.push({"element" :elem[i], "attributes": attrs});    

        };
    }
    if( typeof callback === "function"){ callback(returnedArray) }
    return returnedArray;
};