Scissors.trigger = function(elem,eventName){ 
    var i = 0, j,
        ev,
        elem = typeof elem === "string" ? this.createCache(elem) : 
        (typeof elem === "object" && ! elem.length ? [elem] : elem),
        l = elem.length;
        if(elem.length && typeof eventName === "string"  ){
            ev = this.doc.createEvent('HTMLEvents');
            ev.initEvent(eventName, true, false);
            for ( ; i < l; i++ ) {
                elem[i].dispatchEvent(ev);
            }        
        }
    return elem;    
};