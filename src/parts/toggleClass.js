Scissors.toggleClass = function( elem,value ,callback) {
    var i = 0,
        classes,
        existingIndex,
        elem = typeof elem === "string" ? this.createCache(elem) : 
       (typeof elem === "object" && ! elem.length ? [elem] : elem),

    len = elem.length;
    if ( typeof value === "string" && value ) {
        for ( ; i < len; i++ ) {
            if (elem[i].classList) {
              elem[i].classList.toggle(value);
            } else {
              classes = elem[i].className.split(' ');
              existingIndex = classes.indexOf(value);

              if (existingIndex >= 0)
                classes.splice(existingIndex, 1);
              else
                classes.push(className);

              elem[i].className = classes.join(' ');
            }
        }
        if( typeof callback === "function"){ callback(elem) }
    }
    return elem;
    
};