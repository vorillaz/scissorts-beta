Scissors.fadeIn = function(elem,duration,display,callback){
    var i = 0, j = 0, last,
        elem = typeof elem === "string" ? this.createCache(elem) : 
        (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
        if(typeof display === "undefined" || block == "") display = "block";
        if(typeof duration === "undefined" ) duration = 400;
        if(elem.length && display && duration){
            //helper for async loop
            function fade(j,last) {
                elem[j].style.opacity = +elem[j].style.opacity + (new Date() - last) / duration;
                last = +new Date();

                if (+elem[j].style.opacity < 1) {
                  (window.requestAnimationFrame && requestAnimationFrame(function(){
                    fade(j,last)
                  })) || setTimeout(function(){
                    fade(j,last)
                  }, 16)
                }
            };
            for ( ; i < l; i++ ) {
                j = i;
                if(elem[i].style.display == "none" || elem[i].style.display == ""){
                    elem[i].style.opacity = 0;
                    elem[i].style.display = display;
                    last = +new Date();
                    fade(j,last);
                }
            }
            
        }
        if( typeof callback === "function"){ callback(elem) }
        return elem;
};