Scissors.fadeOut = function(elem,duration, callback){
    var i = 0, j = 0, last,
        elem = typeof elem === "string" ? this.createCache(elem) : 
        (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
        if(typeof duration === "undefined" ) duration = 400;
        if(elem.length && duration){
            //helper for async loop
            function fade(j,last) {
                elem[j].style.opacity = +elem[j].style.opacity - (new Date() - last) / duration;
                last = +new Date();

                if (+elem[j].style.opacity > 0) {
                  (window.requestAnimationFrame && requestAnimationFrame(function(){
                    fade(j,last)
                  })) || setTimeout(function(){
                    fade(j,last)
                  }, 16)
                } else{
                    elem[j].style.display = 'none';
                }
            };
            for ( ; i < l; i++ ) {
                j = i;
                if(elem[i].style.display != "none" && elem[i].style.display != ""){
                    last = +new Date();
                    fade(j,last);
                }
            }
            
        }
        if( typeof callback === "function"){ callback(elem) }
        return elem;
};