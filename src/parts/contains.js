Scissors.contains = function(elem,selector){
    var i = 0,
    elem = typeof elem === "string" ? this.createCache(elem) : 
            (typeof elem === "object" && ! elem.length ? [elem] : elem),
    l = elem.length;
    if(elem.length && typeof selector === "string" && selector){
        for ( ; i < l; i++ ) {
            if(elem[i].querySelector(selector) === null)
                return false;
        }
        return true;
    } 
    return false;

};