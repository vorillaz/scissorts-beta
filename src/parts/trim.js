Scissors.trim= function( text ) {
    return (text == null || !text || typeof text === undefined ) ? "" : text.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
};