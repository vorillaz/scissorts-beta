Scissors.off = function(elem,eventName,func){
    var i = 0, j,
        elem = typeof elem === "string" ? this.createCache(elem) : 
        (typeof elem === "object" && ! elem.length ? [elem] : elem),
        l = elem.length;
    if( typeof func !== "function" || ! elem.length || typeof eventName !== "string")
        return false;
    else{ 
        for ( ; i < l; i++ ) {
            elem[i].removeEventListener(eventName, func);
        }
    }
    return elem;
};