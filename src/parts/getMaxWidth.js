Scissors.getMaxWidth = function(elem,callback) {
	var i = 0,
		getMaxWidth = -1,
		Width = -1,
		selectedElem = [],
        elem = typeof elem === "string" ? this.createCache(elem) : 
               (typeof elem === "object" && ! elem.length ? [elem] : elem),
        l = elem.length;
    if(elem.length){
        for ( ; i < l; i++ ) {
            Width = elem[i].offsetWidth ;
            if(getMaxWidth < Width){
            	getMaxWidth = Width;
            	selectedElem = elem[i];
            }


        }
    }
    if( typeof callback === "function"){ callback({"element": selectedElem, "maxWidth": getMaxWidth }) }
    return {"element" : selectedElem, "maxWidth" : getMaxWidth };
};