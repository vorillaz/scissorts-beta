Scissors.inArray = function (arr,value) {
	for (var i = 0; i < arr.length; i++) {
	    if (typeof value === "object") {
	      if (typeof arr[i] === "object" && value.equals(arr[i]))
	          return i;
	    }
	    else if (arr[i] === value)
	      return i;
	  }
	  return false;
}
  