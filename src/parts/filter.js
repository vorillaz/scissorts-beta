Scissors.filter = function(selector,func){
    var selector = typeof selector === "string" ? this.createCache(selector) : 
        (typeof selector === "object" && ! selector.length ? [selector] : selector);
    if(typeof func === "function" && func && selector){
        try{
         return Array.prototype.filter.call(selector, func);
        } catch(e){
            return false;
        }
    }
    return false;
    
};