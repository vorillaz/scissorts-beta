Scissors.setAttr = function(elem,attribute,setter, callback){
    var i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : 
        (typeof elem === "object" && ! elem.length ? [elem] : elem),
        l = elem.length;
    if(elem.length && typeof attribute === "string" && attribute){
        if( typeof setter !== 'undefined'){
            for ( ; i < l; i++ ) {
                elem[i].setAttribute(attribute, setter);
            }
        }
    }
    if( typeof callback === "function"){ callback(elem) }
    return elem;
};