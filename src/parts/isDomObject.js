Scissors.isDomObject = function(value){
    return (typeof HTMLElement === "object" ? value instanceof HTMLElement : 
                    value && typeof value === "object" && value !== null && value.nodeType === 1 && typeof value.nodeName==="string") ||
        ( typeof Node === "object" ? value instanceof Node : value && typeof value === "object" 
        && typeof value.nodeType === "number" && typeof value.nodeName==="string" );
};