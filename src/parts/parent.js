
Scissors.parent = function( elem, callback ) {
    var i = 0,
        elements = [],
        elem = typeof elem === "string" ? this.createCache(elem) : 
               (typeof elem === "object" && ! elem.length ? [elem] : elem),
        l = elem.length;

    for ( ; i < l; i++ ) {
        elements.push({"element" : elem[i], "parent": elem[i].parentNode })
    }
    if( typeof callback === "function"){ callback(elements) }
    return elements;
};