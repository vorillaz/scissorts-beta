Scissors.documentReady = function(func){
    if( typeof func !== "function")
        return false;
    else{
        if (this.doc.readyState != 'loading'){
            func();
          } else {
            this.doc.addEventListener('DOMContentLoaded', func);
          }
    }
};