Scissors.getWidth = function(elem,callback) {
	var i = 0,
		selectedElems = [],
        elem = typeof elem === "string" ? this.createCache(elem) : 
               (typeof elem === "object" && ! elem.length ? [elem] : elem),
        l = elem.length;
    if(elem.length){
        for ( ; i < l; i++ ) {
            selectedElems.push({ "element":elem[i], "width" : elem[i].offsetWidth  })
        }
    }
    if( typeof callback === "function"){ callback(selectedElems) }
    return selectedElems;
};