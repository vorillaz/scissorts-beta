Scissors.setHtml = function(elem, value, callback){
	var i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : 
        (typeof elem === "object" && ! elem.length ? [elem] : elem),
        l = elem.length,value = value + "";
    if(elem.length && value.length){
    	for ( ; i < l; i++ ) {
                elem[i].innerHTML = value;
            }
    }
    if( typeof callback === "function"){ callback(elem) }
    return elem;
}