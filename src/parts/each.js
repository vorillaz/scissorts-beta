Scissors.each = function(elem,func){
    var elem = typeof elem === "string" ? this.createCache(elem) : 
        (typeof elem === "object" && ! elem.length ? [elem] : elem);
    if( typeof func !== "function" || ! elem.length)
        return false;
    else 
        return Array.prototype.forEach.call(elem, func);
};