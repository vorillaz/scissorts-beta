Scissors.offset = function( elem, callback ) {
    var i = 0,
        elements = [],
        elem = typeof elem === "string" ? this.createCache(elem) : 
               (typeof elem === "object" && ! elem.length ? [elem] : elem),
        l = elem.length;

    for ( ; i < l; i++ ) {
        elements.push({"element" : elem[i], rect: elem[i].getBoundingClientRect({
          top: elem[i].top + this.doc.body.scrollTop,
          left: elem[i].left + this.doc.body.scrollLeft
        })})
    }
    if( typeof callback === "function"){ callback(elements) }
    return elements;
};