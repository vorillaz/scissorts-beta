Scissors.hasClass = function(elem, cl ) {
    var className = " " + cl + " ",
        i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : 
               (typeof elem === "object" && ! elem.length ? [elem] : elem),
        l = elem.length;
    for ( ; i < l; i++ ) {
        if ( elem[i].nodeType === 1 && (" " + elem[i].className + " ").replace( /[\t\r\n\f]/g, " ").indexOf( className ) >= 0 ) {
            return true;
        }
    }

    return false;
};