Scissors.clone = function(elem, callback){
    var i = 0,
    elem = typeof elem === "string" ? this.createCache(elem) : 
           (typeof elem === "object" && ! elem.length ? [elem] : elem),
    l = elem.length;
    for ( ; i < l; i++ ) {
        try{
            elem[i] = elem[i].cloneNode(true);
        } catch(e) { }
    }
    if( typeof callback === "function"){ callback(elem) }
    return elem;

};