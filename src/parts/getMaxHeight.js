Scissors.getMaxHeight = function(elem,callback) {
	var i = 0,
		maxHeight = -1,
		height = -1,
		selectedElem = [],
        elem = typeof elem === "string" ? this.createCache(elem) : 
               (typeof elem === "object" && ! elem.length ? [elem] : elem),
        l = elem.length;
    if(elem.length){
        for ( ; i < l; i++ ) {
            height = elem[i].offsetHeight ;
            if(maxHeight < height){
            	maxHeight = height;
            	selectedElem = elem[i];
            }


        }
    }
    if( typeof callback === "function"){ callback({"element": selectedElem, "maxHeight": maxHeight }) }
    return {"element": selectedElem, "maxHeight": maxHeight };
};