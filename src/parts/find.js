Scissors.find = function(elem,selector,callback){
    var i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : 
        (typeof elem === "object" && ! elem.length ? [elem] : elem),
        l = elem.length;
    if(typeof selector === "string" && selector){
        for ( ; i < l; i++ ) {
            elem[i] = elem[i].querySelectorAll(selector);
        }
        
    }
    if( typeof callback === "function"){ callback(elem) }
    return elem;
};