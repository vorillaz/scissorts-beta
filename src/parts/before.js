Scissors.before = function(elem,value,callback){
    var i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : 
               (typeof elem === "object" && ! elem.length ? [elem] : elem),
        l = elem.length;
    if(elem.length && typeof value === "string" && value){
        for ( ; i < l; i++ ) {
            //dont try to append items to html element failed
            try{
                elem[i].insertAdjacentHTML('beforebegin', value);
            } catch(e) { }
        }
        if( typeof callback === "function"){ callback(elem) }
    }
    return elem;

};