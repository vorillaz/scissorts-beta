Scissors.next = function(elem, callback){
    var i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : 
               (typeof elem === "object" && ! elem.length ? [elem] : elem),
        nextElements = [],
        l = elem.length;
    if( elem.length){
        for ( ; i < l; i++ ) {
            nextElements.push({ "element" : elem[i], "next" : elem[i].nextElementSibling})
        }
        
    }
    if( typeof callback === "function"){ callback(nextElements) }
    return nextElements;

};