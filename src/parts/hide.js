Scissors.hide = function(elem,callback) {
    var i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : 
               (typeof elem === "object" && ! elem.length ? [elem] : elem),
        l = elem.length;
    for ( ; i < l; i++ ) {
        elem[i].style.display = 'none';
    }
    if( typeof callback === "function"){ callback(elem) }
    return elem;
};