Scissors.addClass = function(elem,value, callback){
    var classes, tmpel, cur, clazz, j, finalValue, 
        i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : 
               (typeof elem === "object" && ! elem.length ? [elem] : elem),

            len = elem.length;
        
    if ( typeof value === "string" && value ) {
        classes = ( value || "" ).match( (/\S+/g) ) || [];
        for ( ; i < len; i++ ) {
            tmpel = elem[ i ];
            cur = tmpel.nodeType === 1 && ( tmpel.className ?
                ( " " + tmpel.className + " " ).replace( /[\t\r\n\f]/g, " " ) :
                " "
            );
            if ( cur ) {
                j = 0;
                while ( (clazz = classes[j++]) ) {
                    if ( cur.indexOf( " " + clazz + " " ) < 0 ) {
                        cur += clazz + " ";
                    }
                }
                finalValue = value ? cur.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '') : "";
                if ( tmpel.className !== finalValue ) {
                    tmpel.className = finalValue;
                } 
            }
        }
    }
    if( typeof callback === "function"){ callback(elem) }
    return elem;
};