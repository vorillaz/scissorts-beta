Scissors.getCss = function(elem,styles){
    var i = 0, j,
        elem = typeof elem === "string" ? this.createCache(elem) : 
        (typeof elem === "object" && ! elem.length ? [elem] : elem),
        l = elem.length;
    if(elem.length && typeof styles === "string" && styles){
        var stylesArray =styles.split(" "), stylesOutputElements = [],stylesOutput;
            for ( ; i < l; i++ ) {
                stylesOutput = [];
                
                for ( j = 0; j < stylesArray.length; j++ ) {
                    stylesOutput[stylesArray[j]] = getComputedStyle(elem[i])[stylesArray[j]];   
                }
            stylesOutputElements[i] = { "element" : elem[i], "styles" : stylesOutput}    
            
        }
    } 
    return stylesOutputElements;
};