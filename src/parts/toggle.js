Scissors.toggle = function(elem, callback){
    var i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : 
               (typeof elem === "object" && ! elem.length ? [elem] : elem),
        l = elem.length;
    if(elem.length){
        for ( ; i < l; i++ ) {
            //dont try to append items to html element failed
            if ( elem[i].style.display != 'none' ) {
                elem[i].style.display = 'none';
            } else {
                elem[i].style.display = '';
            }
        }
    }
    if( typeof callback === "function"){ callback(elem) }
    return elem;
};