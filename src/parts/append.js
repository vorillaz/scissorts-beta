Scissors.append = function(elem,value, callback){
    var i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : 
               (typeof elem === "object" && ! elem.length ? [elem] : elem),
        childElements,
        j,
        div = this.doc.createElement('div'),
        l = elem.length;
    if( elem.length && typeof value === "string" && value){
        for ( ; i < l; i++ ) {
            div.innerHTML = value;
            childElements = div.childNodes;
            for (j = 0; j <= childElements.length; j++) {
                elem[i].appendChild(childElements[j]);
            };
            
        }
        
    }
    if( typeof callback === "function"){ callback(elem) }
    return elem;

};