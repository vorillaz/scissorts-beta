window.Scissors = (function( window, document, undefined ) {
    var Scissors = {};
    Scissors.version = "0.5.0",
    Scissors.cache = {},
    Scissors.doc = document;
    
    __Parts__

    /* These are the only dependecies of Scissors */
    Scissors.createCache = Scissors.get = function(query){
        var classes;
          if (!this.cache[query] ) {
            if(/^(#?[\w-]+|\.[\w-.]+)$/.test(query)){
                switch(query.charAt(0)){
                    case '#':
                        return this.cache[query] = [this.doc.getElementById(query.substr(1))];
                    case '.':
                        classes = query.substr(1).replace(/\./g, ' ');
                        return this.cache[query] = [].slice.call(this.doc.getElementsByClassName(classes));
                     default:
                        return this.cache[query] = [].slice.call(this.doc.getElementsByTagName(query)); 
                }
            }
            return this.cache[query] = [].slice.call(this.doc.querySelectorAll(query));
          }
          return this.cache[query];
    };
    return Scissors;
})(this, this.document);