/**
 * Scissors.js (Custom build)
 * (c) 2015 Theodore Vorillas & Editors
 * Build: addClass,append,before,clone,contains,documentReady
 * Generated: Sun, 07 Jun 2015 20:25:50 GMT
 * @version v0.5.0
 * @link http://github.com/vorillaz/scissors/
 * @license MIT | http://opensource.org/licenses/mit-license.html
 */
window.Scissors = (function (window, document, undefined) {
  var Scissors = {};
  Scissors.version = "0.5.0", Scissors.cache = {}, Scissors.doc = document;

  Scissors.addClass = function (elem, value, callback) {
    var classes, tmpel, cur, clazz, j, finalValue, i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        
        
        len = elem.length;

    if (typeof value === "string" && value) {
      classes = (value || "").match((/\S+/g)) || [];
      for (; i < len; i++) {
        tmpel = elem[i];
        cur = tmpel.nodeType === 1 && (tmpel.className ? (" " + tmpel.className + " ").replace(/[\t\r\n\f]/g, " ") : " ");
        if (cur) {
          j = 0;
          while ((clazz = classes[j++])) {
            if (cur.indexOf(" " + clazz + " ") < 0) {
              cur += clazz + " ";
            }
          }
          finalValue = value ? cur.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '') : "";
          if (tmpel.className !== finalValue) {
            tmpel.className = finalValue;
          }
        }
      }
    }
    if (typeof callback === "function") {
      callback(elem)
    }
    return elem;
  };
  Scissors.append = function (elem, value, callback) {
    var i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        childElements, j, div = this.doc.createElement('div'),
        l = elem.length;
    if (elem.length && typeof value === "string" && value) {
      for (; i < l; i++) {
        div.innerHTML = value;
        childElements = div.childNodes;
        for (j = 0; j <= childElements.length; j++) {
          elem[i].appendChild(childElements[j]);
        };

      }

    }
    if (typeof callback === "function") {
      callback(elem)
    }
    return elem;

  };
  Scissors.before = function (elem, value, callback) {
    var i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
    if (elem.length && typeof value === "string" && value) {
      for (; i < l; i++) {
        //dont try to append items to html element failed
        try {
          elem[i].insertAdjacentHTML('beforebegin', value);
        } catch (e) {}
      }
      if (typeof callback === "function") {
        callback(elem)
      }
    }
    return elem;

  };
  Scissors.clone = function (elem, callback) {
    var i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
    for (; i < l; i++) {
      try {
        elem[i] = elem[i].cloneNode(true);
      } catch (e) {}
    }
    if (typeof callback === "function") {
      callback(elem)
    }
    return elem;

  };
  Scissors.contains = function (elem, selector) {
    var i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
    if (elem.length && typeof selector === "string" && selector) {
      for (; i < l; i++) {
        if (elem[i].querySelector(selector) === null) return false;
      }
      return true;
    }
    return false;

  };
  Scissors.documentReady = function (func) {
    if (typeof func !== "function") return false;
    else {
      if (this.doc.readyState != 'loading') {
        func();
      } else {
        this.doc.addEventListener('DOMContentLoaded', func);
      }
    }
  };

  /* These are the only dependecies of Scissors */
  Scissors.createCache = Scissors.get = function (query) {
    var classes;
    if (!this.cache[query]) {
      if (/^(#?[\w-]+|\.[\w-.]+)$/.test(query)) {
        switch (query.charAt(0)) {
        case '#':
          return this.cache[query] = [this.doc.getElementById(query.substr(1))];
        case '.':
          classes = query.substr(1).replace(/\./g, ' ');
          return this.cache[query] = [].slice.call(this.doc.getElementsByClassName(classes));
        default:
          return this.cache[query] = [].slice.call(this.doc.getElementsByTagName(query));
        }
      }
      return this.cache[query] = [].slice.call(this.doc.querySelectorAll(query));
    }
    return this.cache[query];
  };
  return Scissors;
})(this, this.document);