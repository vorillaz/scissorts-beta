var requestsCnt = 0, random = 0; 
Scissors.documentReady(function(){
	runTests();

	Scissors.on("#fifften", "click", function(){
		runMultiple(15);
	});

	Scissors.on("#thirty", "click", function(){
		runMultiple(30);
	});
	Scissors.on("#hun", "click", function(){
		runMultiple(100);
	});

	Scissors.on("#show", "click", function(){
		var perItems = window.performance.getEntriesByType('measure'), weight = 0;
		for (var i = 0; i < perItems.length; ++i) {
		  var req = perItems[i];
		  weight +=req.duration;
		  console.log('Round: ' + req.name + ' took ' + req.duration + 'ms');
		}
		Scissors.setHtml("#info",'Average time of execution: ' + weight / perItems.length + " ms. Open your console for more <b>infos</b>.");
		Scissors.show("#info");
	});
});

var runMultiple = function(num){
	Scissors.hide("#info");
	random = Math.random();
	for (var i = 0; i < num; i++) {
		runTests();
	};
}

var runTests = function(){
	window.performance.mark('start_sc');
	requestsCnt++;
	Scissors.setHtml("b","Num of test: "+requestsCnt);
	Scissors.addClass(".test","custom");
	Scissors.hide(".test");
	Scissors.show(".test");
	Scissors.setHtml(".test",random);
	window.performance.mark('end_sc');
	window.performance.measure('test_num' + requestsCnt, 'start_sc', 'end_sc');
}