var requestsCnt = 0, random = 0;
$( document ).ready(function() {
	runTests();

	$("#fifften").click(function(){
		runMultiple(15);
	});
	$("#thirty").click(function(){
		runMultiple(30);
	});
	$("#hun").click(function(){
		runMultiple(100);
	});
	$("#show").click(function(){
		var perItems = window.performance.getEntriesByType('measure'), weight = 0;
		for (var i = 0; i < perItems.length; ++i) {
		  var req = perItems[i];
		  weight +=req.duration;
		  console.log('Round: ' + req.name + ' took ' + req.duration + 'ms');
		}
		$("#info").show();
		$("#info").html('Average time of execution: ' + weight / perItems.length + " ms. Open your console for more <b>infos</b>.");
	});
	
});
var runMultiple = function(num){
	$("#info").hide();
	random = Math.random();
	for (var i = 0; i < num; i++) {
		runTests();
	};
}

var runTests = function(){
	console.log(1)
	window.performance.mark('start_jq');
	requestsCnt++;
	$("b").html("Num of test: "+requestsCnt);
	$(".test").addClass("custom");
	$(".test").hide();
	$(".test").show();
	$(".test").html(random);
	window.performance.mark('end_jq');
	window.performance.measure('test_num' + requestsCnt, 'start_jq', 'end_jq');
}
