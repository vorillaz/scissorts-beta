var times = 0, elapsed, rounds = 15	 , counter = 0; 
Scissors.documentReady(function(){
	runTests();
	Scissors.on("button","click",function(){
		counter += rounds;
		for (var i = 0; i <= rounds; i++) {
			elapsed =  runTests();
			times += elapsed;
		};
		Scissors.setHtml("i","Scissors had test had run "+counter+" times, average time of execution : " + times/counter+" ms")
	});
});


var runTests = function(){
	var start = new Date().getTime();
	Scissors.addClass("div","custom");
	if(!Scissors.contains("div","em"))
		Scissors.append("div","<em>I was appended ;)</em>");
	else
		Scissors.setHtml("em",'I was manipulated ' + counter + " times");

	Scissors.removeClass("div","custom");
	Scissors.addClass("div","custom");
	Scissors.setAttr("div","data-custom","custom");
	var end = new Date().getTime();
	var time = end - start;
	Scissors.setHtml("b",'Execution time: ' + time +" ms");
	return time;
}