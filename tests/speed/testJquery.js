var times = 0, elapsed, rounds = 15 , counter = 0; 
$( document ).ready(function() {
	runTests();
	$("button").click(function(){
		counter += rounds;
		for (var i = 0; i <= rounds; i++) {
			elapsed =  runTests();
			times += elapsed;
		};

		$("i").html("jQuery test had run "+counter+" times, average time of execution : " + times/counter+" ms")
	})
});

var runTests = function(){
	var start = new Date().getTime();
	$("div").addClass("custom");
	//WTF JQUERY! if(! $( "div" ).has( "em" ).length) THIS IS PERFOMANCE KILLER
	if(! $( "div" ).find( "em" ).length)
		$("div").append("<em>I was appended ;)</em>");
	else
		$("em").html('I was manipulated ' + counter + " times");

	$("div.custom").removeClass("custom");
	$("div").addClass("custom");
	$("div.custom").attr("data-custom","custom");
    var end = new Date().getTime();
	var time = end - start;
	$("b").html('Execution time: ' + time + " ms");
	return time;
}
