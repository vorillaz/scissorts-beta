var $charts,t1,t2,overall;
var data = {
        chart: {
        renderTo: 'container',
        
    },
        title: {
            text: '',
            x: -20 //center
        },
        xAxis: {
            categories: []
        },
        yAxis: {
            title: {
                text: 'Milliseconds'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: 'ms'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'jQuery',
            data: []
        },
                {
            name: 'Scissors',
            data: []
        }]
    }

var requestsCnt = 0;
$( document ).ready(function() {
	runonce();
	Scissors.on("button", "click", function(){
		
		for (var i = 0; i< 5; i++) {
			runJquery();
			runScissors();
		};
		
	})
});


var runMultiple = function(){

}

var runonce = function(){
	$charts = new Highcharts.Chart(data);
	for (var i = 0; i< 5; i++) {
		runJquery();
		runScissors();
	}
}

var runJquery = function(){
	requestsCnt++;
	t1 = window.performance.now();
	$(".test").removeClass("custom");
	$(".test").addClass("custom");
	$(".test").hide();
	$(".test").show();
	$(".test").html(requestsCnt);
	t2 = window.performance.now();
	overall = t2 -t1;
	data.series[0].data.push(overall)
    $charts.series[0].setData(data.series[0].data)
	
}
var runScissors = function(){
	requestsCnt++;
	t1 = window.performance.now();
	Scissors.removeClass(".test","custom");
	Scissors.addClass(".test","custom");
	Scissors.hide(".test");
	Scissors.show(".test");
	Scissors.setHtml(".test",requestsCnt);
	t2 = window.performance.now();
	overall = t2 -t1;
	data.series[1].data.push(overall)
	
    $charts.series[1].setData(data.series[1].data)
}