window.Scissors = (function (window, document, undefined) {
  var Scissors = {};
  Scissors.version = "0.5.0", Scissors.cache = {}, Scissors.doc = document;

  Scissors.addClass = function (elem, value, callback) {
    var classes, tmpel, cur, clazz, j, finalValue, i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        
        
        len = elem.length;

    if (typeof value === "string" && value) {
      classes = (value || "").match((/\S+/g)) || [];
      for (; i < len; i++) {
        tmpel = elem[i];
        cur = tmpel.nodeType === 1 && (tmpel.className ? (" " + tmpel.className + " ").replace(/[\t\r\n\f]/g, " ") : " ");
        if (cur) {
          j = 0;
          while ((clazz = classes[j++])) {
            if (cur.indexOf(" " + clazz + " ") < 0) {
              cur += clazz + " ";
            }
          }
          finalValue = value ? cur.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '') : "";
          if (tmpel.className !== finalValue) {
            tmpel.className = finalValue;
          }
        }
      }
    }
    if (typeof callback === "function") {
      callback(elem)
    }
    return elem;
  };
  Scissors.after = function (elem, value, callback) {
    var i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
    if (elem.length && typeof value === "string" && value) {
      for (; i < l; i++) {
        //dont try to append items to html element failed
        elem[i].insertAdjacentHTML('afterend', value);
      }
      if (typeof callback === "function") {
        callback(elem)
      }
    }
    return elem;

  };
  Scissors.append = function (elem, value, callback) {
    var i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        childElements, j, div = this.doc.createElement('div'),
        l = elem.length;
    if (elem.length && typeof value === "string" && value) {
      for (; i < l; i++) {
        div.innerHTML = value;
        childElements = div.childNodes;
        for (j = 0; j <= childElements.length; j++) {
          elem[i].appendChild(childElements[j]);
        };

      }

    }
    if (typeof callback === "function") {
      callback(elem)
    }
    return elem;

  };
  Scissors.before = function (elem, value, callback) {
    var i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
    if (elem.length && typeof value === "string" && value) {
      for (; i < l; i++) {
        //dont try to append items to html element failed
        try {
          elem[i].insertAdjacentHTML('beforebegin', value);
        } catch (e) {}
      }
      if (typeof callback === "function") {
        callback(elem)
      }
    }
    return elem;

  };
  Scissors.clone = function (elem, callback) {
    var i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
    for (; i < l; i++) {
      try {
        elem[i] = elem[i].cloneNode(true);
      } catch (e) {}
    }
    if (typeof callback === "function") {
      callback(elem)
    }
    return elem;

  };
  Scissors.contains = function (elem, selector) {
    var i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
    if (elem.length && typeof selector === "string" && selector) {
      for (; i < l; i++) {
        if (elem[i].querySelector(selector) === null) return false;
      }
      return true;
    }
    return false;

  };
  Scissors.documentReady = function (func) {
    if (typeof func !== "function") return false;
    else {
      if (this.doc.readyState != 'loading') {
        func();
      } else {
        this.doc.addEventListener('DOMContentLoaded', func);
      }
    }
  };
  Scissors.each = function (elem, func) {
    var elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem);
    if (typeof func !== "function" || !elem.length) return false;
    else return Array.prototype.forEach.call(elem, func);
  };
  Scissors.fadeIn = function (elem, duration, display, callback) {
    var i = 0,
        j = 0,
        last, elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
    if (typeof display === "undefined" || block == "") display = "block";
    if (typeof duration === "undefined") duration = 400;
    if (elem.length && display && duration) {
      //helper for async loop


      function fade(j, last) {
        elem[j].style.opacity = +elem[j].style.opacity + (new Date() - last) / duration;
        last = +new Date();

        if (+elem[j].style.opacity < 1) {
          (window.requestAnimationFrame && requestAnimationFrame(function () {
            fade(j, last)
          })) || setTimeout(function () {
            fade(j, last)
          }, 16)
        }
      };
      for (; i < l; i++) {
        j = i;
        if (elem[i].style.display == "none" || elem[i].style.display == "") {
          elem[i].style.opacity = 0;
          elem[i].style.display = display;
          last = +new Date();
          fade(j, last);
        }
      }

    }
    if (typeof callback === "function") {
      callback(elem)
    }
    return elem;
  };
  Scissors.fadeOut = function (elem, duration, callback) {
    var i = 0,
        j = 0,
        last, elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
    if (typeof duration === "undefined") duration = 400;
    if (elem.length && duration) {
      //helper for async loop


      function fade(j, last) {
        elem[j].style.opacity = +elem[j].style.opacity - (new Date() - last) / duration;
        last = +new Date();

        if (+elem[j].style.opacity > 0) {
          (window.requestAnimationFrame && requestAnimationFrame(function () {
            fade(j, last)
          })) || setTimeout(function () {
            fade(j, last)
          }, 16)
        } else {
          elem[j].style.display = 'none';
        }
      };
      for (; i < l; i++) {
        j = i;
        if (elem[i].style.display != "none" && elem[i].style.display != "") {
          last = +new Date();
          fade(j, last);
        }
      }

    }
    if (typeof callback === "function") {
      callback(elem)
    }
    return elem;
  };
  Scissors.filter = function (selector, func) {
    var selector = typeof selector === "string" ? this.createCache(selector) : (typeof selector === "object" && !selector.length ? [selector] : selector);
    if (typeof func === "function" && func && selector) {
      try {
        return Array.prototype.filter.call(selector, func);
      } catch (e) {
        return false;
      }
    }
    return false;

  };
  Scissors.find = function (elem, selector, callback) {
    var i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
    if (typeof selector === "string" && selector) {
      for (; i < l; i++) {
        elem[i] = elem[i].querySelectorAll(selector);
      }

    }
    if (typeof callback === "function") {
      callback(elem)
    }
    return elem;
  };
  Scissors.getAttr = function (elem, attribute, callback) {
    var i = 0,
        tmpobj = {},
        attrs = [],
        returnedArray = [],
        arrayofAttrs = [],
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
    if (elem.length && typeof attribute === "string" && attribute) {
      arrayofAttrs = attribute.split(",");


      for (; i < l; i++) {
        tmpobj = {};
        tmpobj.element = elem[i];
        attrs = [];
        for (var j = arrayofAttrs.length - 1; j >= 0; j--) {
          attrs[arrayofAttrs[j]] = elem[i].getAttribute(arrayofAttrs[j]);
        }
        tmpobj.attributes = attrs;
        returnedArray.push({
          "element": elem[i],
          "attributes": attrs
        });

      };
    }
    if (typeof callback === "function") {
      callback(returnedArray)
    }
    return returnedArray;
  };
  Scissors.getCache = function (query) {
    return (!this.cache[query]) ? false : this.cache[query];
  };
  Scissors.getCss = function (elem, styles) {
    var i = 0,
        j, elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
    if (elem.length && typeof styles === "string" && styles) {
      var stylesArray = styles.split(" "),
          stylesOutputElements = [],
          stylesOutput;
      for (; i < l; i++) {
        stylesOutput = [];

        for (j = 0; j < stylesArray.length; j++) {
          stylesOutput[stylesArray[j]] = getComputedStyle(elem[i])[stylesArray[j]];
        }
        stylesOutputElements[i] = {
          "element": elem[i],
          "styles": stylesOutput
        }

      }
    }
    return stylesOutputElements;
  };
  Scissors.getHeight = function (elem, callback) {
    var i = 0,
        selectedElems = [],
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
    if (elem.length) {
      for (; i < l; i++) {
        selectedElems.push({
          "element": elem[i],
          "height": elem[i].offsetHeight
        })
      }
    }
    if (typeof callback === "function") {
      callback(selectedElems)
    }
    return selectedElems;
  };
  Scissors.getMaxHeight = function (elem, callback) {
    var i = 0,
        maxHeight = -1,
        height = -1,
        selectedElem = [],
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
    if (elem.length) {
      for (; i < l; i++) {
        height = elem[i].offsetHeight;
        if (maxHeight < height) {
          maxHeight = height;
          selectedElem = elem[i];
        }


      }
    }
    if (typeof callback === "function") {
      callback({
        "element": selectedElem,
        "maxHeight": maxHeight
      })
    }
    return {
      "element": selectedElem,
      "maxHeight": maxHeight
    };
  };
  Scissors.getMaxWidth = function (elem, callback) {
    var i = 0,
        getMaxWidth = -1,
        Width = -1,
        selectedElem = [],
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
    if (elem.length) {
      for (; i < l; i++) {
        Width = elem[i].offsetWidth;
        if (getMaxWidth < Width) {
          getMaxWidth = Width;
          selectedElem = elem[i];
        }


      }
    }
    if (typeof callback === "function") {
      callback({
        "element": selectedElem,
        "maxWidth": getMaxWidth
      })
    }
    return {
      "element": selectedElem,
      "maxWidth": getMaxWidth
    };
  };
  Scissors.getWidth = function (elem, callback) {
    var i = 0,
        selectedElems = [],
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
    if (elem.length) {
      for (; i < l; i++) {
        selectedElems.push({
          "element": elem[i],
          "width": elem[i].offsetWidth
        })
      }
    }
    if (typeof callback === "function") {
      callback(selectedElems)
    }
    return selectedElems;
  };
  Scissors.hasClass = function (elem, cl) {
    var className = " " + cl + " ",
        i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
    for (; i < l; i++) {
      if (elem[i].nodeType === 1 && (" " + elem[i].className + " ").replace(/[\t\r\n\f]/g, " ").indexOf(className) >= 0) {
        return true;
      }
    }

    return false;
  };
  Scissors.hide = function (elem, callback) {
    var i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
    for (; i < l; i++) {
      elem[i].style.display = 'none';
    }
    if (typeof callback === "function") {
      callback(elem)
    }
    return elem;
  };
  Scissors.inArray = function (arr, value) {
    for (var i = 0; i < arr.length; i++) {
      if (typeof value === "object") {
        if (typeof arr[i] === "object" && value.equals(arr[i])) return i;
      }
      else if (arr[i] === value) return i;
    }
    return false;
  }

  Scissors.isDomObject = function (value) {
    return (typeof HTMLElement === "object" ? value instanceof HTMLElement : value && typeof value === "object" && value !== null && value.nodeType === 1 && typeof value.nodeName === "string") || (typeof Node === "object" ? value instanceof Node : value && typeof value === "object" && typeof value.nodeType === "number" && typeof value.nodeName === "string");
  };
  Scissors.next = function (elem, callback) {
    var i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        nextElements = [],
        l = elem.length;
    if (elem.length) {
      for (; i < l; i++) {
        nextElements.push({
          "element": elem[i],
          "next": elem[i].nextElementSibling
        })
      }

    }
    if (typeof callback === "function") {
      callback(nextElements)
    }
    return nextElements;

  };
  Scissors.off = function (elem, eventName, func) {
    var i = 0,
        j, elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
    if (typeof func !== "function" || !elem.length || typeof eventName !== "string") return false;
    else {
      for (; i < l; i++) {
        elem[i].removeEventListener(eventName, func);
      }
    }
    return elem;
  };
  Scissors.offset = function (elem, callback) {
    var i = 0,
        elements = [],
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;

    for (; i < l; i++) {
      elements.push({
        "element": elem[i],
        rect: elem[i].getBoundingClientRect({
          top: elem[i].top + this.doc.body.scrollTop,
          left: elem[i].left + this.doc.body.scrollLeft
        })
      })
    }
    if (typeof callback === "function") {
      callback(elements)
    }
    return elements;
  };
  Scissors.on = function (elem, eventName, func) {
    var i = 0,
        j, elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
    if (typeof func !== "function" || !elem.length || typeof eventName !== "string") return false;
    else {
      for (; i < l; i++) {
        elem[i].addEventListener(eventName, func);
      }
    }
    return elem;
  };

  Scissors.parent = function (elem, callback) {
    var i = 0,
        elements = [],
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;

    for (; i < l; i++) {
      elements.push({
        "element": elem[i],
        "parent": elem[i].parentNode
      })
    }
    if (typeof callback === "function") {
      callback(elements)
    }
    return elements;
  };
  Scissors.prepend = function (elem, value, callback) {
    var i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        childElements, j = 0,
        div, l = elem.length;
    if (elem.length && typeof value === "string" && value) {
      div = this.doc.createElement('div');
      for (; i < l; i++) {
        div.innerHTML = value;
        childElements = div.childNodes;
        for (j = 0; j <= childElements.length; j++) {
          elem[i].insertBefore(childElements[j], elem[i].firstChild);
        };

      }
    }
    if (typeof callback === "function") {
      callback(elem)
    }
    return elem;

  };
  Scissors.remove = function (elem, callback) {
    var i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
    if (elem.length) {
      for (; i < l; i++) {
        //dont try to append items to html element failed
        try {
          elem[i].parentNode.removeChild(elem[i]);
        } catch (e) {}

      }
      if (typeof callback === "function") {
        callback(elem)
      }
    }
    return elem;

  };
  Scissors.removeClass = function (elem, value, callback) {
    var classes, tmpel, cur, clazz, j, finalValue, i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        len = elem.length;

    if (typeof value === "string" && value) {
      classes = (value || "").match((/\S+/g)) || [];

      for (; i < len; i++) {
        tmpel = elem[i];
        cur = tmpel.nodeType === 1 && (tmpel.className ? (" " + tmpel.className + " ").replace(/[\t\r\n\f]/g, " ") : "");

        if (cur) {
          j = 0;
          while ((clazz = classes[j++])) {
            while (cur.indexOf(" " + clazz + " ") >= 0) {
              cur = cur.replace(" " + clazz + " ", " ");
            }
          }

          // only assign if different to avoid unneeded rendering.
          finalValue = value ? Scissors.trim(cur) : "";
          if (tmpel.className !== finalValue) {
            tmpel.className = finalValue;
          }
        }
      }
      if (typeof callback === "function") {
        callback(elem)
      }
    }

    return elem;
  };
  Scissors.setAttr = function (elem, attribute, setter, callback) {
    var i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
    if (elem.length && typeof attribute === "string" && attribute) {
      if (typeof setter !== 'undefined') {
        for (; i < l; i++) {
          elem[i].setAttribute(attribute, setter);
        }
      }
    }
    if (typeof callback === "function") {
      callback(elem)
    }
    return elem;
  };
  Scissors.setHtml = function (elem, value, callback) {
    var i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length,
        value = value + "";
    if (elem.length && value.length) {
      for (; i < l; i++) {
        elem[i].innerHTML = value;
      }
    }
    if (typeof callback === "function") {
      callback(elem)
    }
    return elem;
  }
  Scissors.show = function (elem, callback) {
    var i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
    for (; i < l; i++) {
      elem[i].style.display = '';
    }
    if (typeof callback === "function") {
      callback(elem)
    }
    return elem;
  };
  Scissors.toggle = function (elem, callback) {
    var i = 0,
        elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
    if (elem.length) {
      for (; i < l; i++) {
        //dont try to append items to html element failed
        if (elem[i].style.display != 'none') {
          elem[i].style.display = 'none';
        } else {
          elem[i].style.display = '';
        }
      }
    }
    if (typeof callback === "function") {
      callback(elem)
    }
    return elem;
  };
  Scissors.toggleClass = function (elem, value, callback) {
    var i = 0,
        classes, existingIndex, elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        
        
        len = elem.length;
    if (typeof value === "string" && value) {
      for (; i < len; i++) {
        if (elem[i].classList) {
          elem[i].classList.toggle(value);
        } else {
          classes = elem[i].className.split(' ');
          existingIndex = classes.indexOf(value);

          if (existingIndex >= 0) classes.splice(existingIndex, 1);
          else classes.push(className);

          elem[i].className = classes.join(' ');
        }
      }
      if (typeof callback === "function") {
        callback(elem)
      }
    }
    return elem;

  };
  Scissors.trigger = function (elem, eventName) {
    var i = 0,
        j, ev, elem = typeof elem === "string" ? this.createCache(elem) : (typeof elem === "object" && !elem.length ? [elem] : elem),
        l = elem.length;
    if (elem.length && typeof eventName === "string") {
      ev = this.doc.createEvent('HTMLEvents');
      ev.initEvent(eventName, true, false);
      for (; i < l; i++) {
        elem[i].dispatchEvent(ev);
      }
    }
    return elem;
  };
  Scissors.trim = function (text) {
    return (text == null || !text || typeof text === undefined) ? "" : text.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
  };
  Scissors.windowLoad = function (func) {
    var oldonload = window.onload;
    if (typeof window.onload != 'function') {
      window.onload = func;

    } else {
      window.onload = function () {
        oldonload();
        func();
      }
    }

  };

  /* These are the only dependecies of Scissors */
  Scissors.createCache = Scissors.get = function (query) {
    var classes;
    if (!this.cache[query]) {
      if (/^(#?[\w-]+|\.[\w-.]+)$/.test(query)) {
        switch (query.charAt(0)) {
        case '#':
          return this.cache[query] = [this.doc.getElementById(query.substr(1))];
        case '.':
          classes = query.substr(1).replace(/\./g, ' ');
          return this.cache[query] = [].slice.call(this.doc.getElementsByClassName(classes));
        default:
          return this.cache[query] = [].slice.call(this.doc.getElementsByTagName(query));
        }
      }
      return this.cache[query] = [].slice.call(this.doc.querySelectorAll(query));
    }
    return this.cache[query];
  };
  return Scissors;
})(this, this.document);